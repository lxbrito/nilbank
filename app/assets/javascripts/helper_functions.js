var round_2, round_2f;

round_2 = function(num) {
    return Math.round(num*100)/100 ;
}

round_2f = function(num) {
    return round_2(num).toFixed(2) ;
}