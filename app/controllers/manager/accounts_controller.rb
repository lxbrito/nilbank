module Manager
  class AccountsController < Manager::ApplicationController
    before_action :set_account, only:[ :show]
    def index
      @accounts= Account.presentable
    end

    def new
      @account = AccountForm.new(Account.new)
    end

    def create
      @account= AccountForm.new(Account.new)
      if @account.validate(account_params)
        #TODO fix this code, this is a workaround
        Account.create!(name: @account.name,
                        acc_number: @account.acc_number,
                        role: @account.role,
                        password:@account.password,
                        password_confirmation: @account.password_confirmation)
        redirect_to manager_accounts_path
      else
        render :new
      end
    end

    def edit
      @account= AccountForm.new(Account.find(params[:id]))
    end

    def update
      acc = Account.find(params[:id])
      @account= AccountForm.new(acc)
      if @account.validate(account_params)
        #TODO fix this code, this is a workaround
        acc.name = @account.name
        acc.role = @account.role
        acc.password = @account.password if @account.password.present?
        acc.password_confirmation = @account.password_confirmation if @account.password.present?
        acc.save!
        redirect_to manager_accounts_path, notice:"Conta alterada com sucesso"
      else
        render :edit
      end
    end

    def show
      @acc = @account.decorate
    end

    private
    def set_account
      @account= Account.find(params[:id])
    end

    def account_params
      params.require(:account).permit(:name, :acc_number, :password, :password_confirmation, :role)
    end
  end
end