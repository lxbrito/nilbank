module Manager
  class ApplicationController < ActionController::Base

    rescue_from Exception, :with => :general_error
    protect_from_forgery with: :exception

    layout 'manager'
    before_action :authenticate_account!
    before_action :authenticate_manager!

    private
    def authenticate_manager!
      return redirect_to current_account.decorate.root_path unless current_account.manager?
    end

    def general_error(exception)
      Rails.logger.error(exception.message)
      Rails.logger.error(exception.backtrace.join("\n"))
      return redirect_to current_account.decorate.root_path, alert: "Infelizmente não foi possível realizar a transação.\nPor favor tente novamente em alguns minutos."
    end
  end
end