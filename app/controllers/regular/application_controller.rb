module Regular
  class ApplicationController < ActionController::Base
    rescue_from Exception, :with => :general_error
    rescue_from InsuficientFundsException, :with => :no_funds
    protect_from_forgery with: :exception

    layout 'regular'
    before_action :authenticate_account!
    before_action :authenticate_regular!

    private
    def authenticate_regular!
      return redirect_to current_account.decorate.root_path unless current_account.regular?
    end

    def no_funds
      @acc= current_account.decorate
      flash.alert= "Você não possui saldo suficiente para essa operação."
      render :new
    end

    def general_error(exception)
      Rails.logger.error(exception.message)
      Rails.logger.error(exception.backtrace.join("\n"))
      return redirect_to current_account.decorate.root_path, alert: "Infelizmente não foi possível realizar a transação.\nPor favor tente novamente em alguns minutos."
    end
  end
end