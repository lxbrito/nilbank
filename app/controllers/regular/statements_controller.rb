class Regular::StatementsController < Regular::ApplicationController
  def index
    @acc=current_account.decorate
    @operations =  FinancialOperationDecorator.decorate_collection(current_account.financial_operations.last(20))
  end
end
