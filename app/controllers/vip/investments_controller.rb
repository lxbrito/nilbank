class Vip::InvestmentsController < Vip::ApplicationController
  rescue_from InsuficientStocksException, :with => :not_enough_stocks
  # I know, I should use a proper route name
  # buy Stocks
  def index
    @acc=current_account.decorate
    @my_stocks = StockDecorator.decorate_collection(@acc.stocks)
    @available_stocks = Portfolio::STOCK_SYMBOLS.map{|symbol| Stock.new(stock_symbol: symbol).decorate}
  end

  def new
    @acc=current_account.decorate
    @investment = InvestmentForm.new(FinancialOperation.new)
    @investment.stock_symbol = params[:stock]
    return redirect_to vip_investments_path, alert: "Ação não disponível!" unless Portfolio::STOCK_SYMBOLS.include?(@investment.stock_symbol)
  end

  # I know, I should use a proper route name
  # buy Stocks
  def create
    @investment = InvestmentForm.new(FinancialOperation.new)
    if @investment.validate(params[:investment])
      Investment.new(current_account).buy(@investment.qtd.to_i, @investment.stock_symbol)
      ##
      return redirect_to vip_investments_path, notice:"Compra de ações efetuada com sucesso."
    else
      @acc=current_account.decorate
      render :new
    end
  end

  # I know, I should use a proper route name
  # Sell Stocks
  def edit
    @acc=current_account.decorate
    @investment = InvestmentForm.new(FinancialOperation.new)
    @stock = Stock.find(params[:id])
    @investment.stock_symbol= @stock.stock_symbol
  end

  # I know, I should use a proper route name
  # Sell Stocks
  def update
    @investment = InvestmentForm.new(FinancialOperation.new)
    if @investment.validate(params[:investment])
      @stock = Stock.find(params[:id])
      Investment.new(current_account).sell(@investment.qtd.to_i, @stock.stock_symbol)
      ##
      return redirect_to vip_investments_path, notice:"Venda de ações efetuada com sucesso."
    else
      @acc=current_account.decorate
      @investment.stock_symbol= @stock.stock_symbol
      render :new
    end
  end

  private
  def not_enough_stocks
    @acc= current_account.decorate
    return redirect_to vip_investments_path, alert: "Você não possui ações suficientes para essa operação."
  end
end
