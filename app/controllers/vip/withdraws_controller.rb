class Vip::WithdrawsController < Vip::ApplicationController
  def new
    @acc=current_account.decorate
    @withdraw = WithdrawForm.new(FinancialOperation.new)
  end

  def create
    @withdraw = WithdrawForm.new(FinancialOperation.new)
    if @withdraw.validate(params[:withdraw])
      Withdraw.new(current_account).call(@withdraw.ammount.to_d)
      ##
      return redirect_to current_account.decorate.root_path, notice: "Saque efetuado com sucesso!"
    else
      @acc=current_account.decorate
      render :new
    end
  end
end
