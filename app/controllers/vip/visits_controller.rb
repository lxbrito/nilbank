class Vip::VisitsController < Vip::ApplicationController
  def index
    Visit.new(current_account).call
    redirect_to current_account.decorate.root_path, notice: "Visita solicitada"
  end
end
