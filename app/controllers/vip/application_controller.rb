module Vip
  class ApplicationController < ActionController::Base
    rescue_from Exception, :with => :general_error

    protect_from_forgery with: :exception

    layout 'vip'
    before_action :authenticate_account!
    before_action :authenticate_vip!

    private
    def authenticate_vip!
      return redirect_to current_account.decorate.root_path unless current_account.vip?
    end

    def general_error(exception)
      Rails.logger.error(exception.message)
      Rails.logger.error(exception.backtrace.join("\n"))
      return redirect_to current_account.decorate.root_path, alert: "Infelizmente não foi possível realizar a transação.\nPor favor tente novamente em alguns minutos."
    end
  end
end