class Vip::DepositsController < Vip::ApplicationController
  def new
    @acc= current_account.decorate
    @deposit = DepositForm.new(FinancialOperation.new)
  end

  def create
    @deposit = DepositForm.new(FinancialOperation.new)
    if @deposit.validate(params[:deposit])
      Deposit.new(current_account).call(@deposit.ammount.to_d)
      ##
      return redirect_to current_account.decorate.root_path, notice: "Deposito efetuado com sucesso!"
    else
      @acc=current_account.decorate
      render :new
    end
  end
end
