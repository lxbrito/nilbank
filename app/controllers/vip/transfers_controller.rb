class Vip::TransfersController < Vip::ApplicationController
  def new
    @acc=current_account.decorate
    @transfer = TransferForm.new(FinancialOperation.new)
  end

  def create
    @transfer = TransferForm.new(FinancialOperation.new(account:current_account))
    if @transfer.validate(params[:transfer])
      Transfer.new(current_account).call(@transfer.ammount.to_d, @transfer.destination_account)
      ##
      return redirect_to current_account.decorate.root_path, notice: "Transferência realizada com sucesso!"
    else
      @acc=current_account.decorate
      render :new
    end
  end
end
