# == Schema Information
#
# Table name: financial_transactions
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FinancialTransaction < ActiveRecord::Base

  def self.single(account, &block)
    account.with_lock do
      t = self.create
      op = account.build_operation(t.id)
      yield op
    end
  end

  def self.double(account1, account2, &block)
    account1.with_lock do
      account2.lock!(true)
      t = self.create
      op1 = account1.build_operation(t.id)
      op2 = account2.build_operation(t.id)
      yield op1, op2
    end
  end
end
