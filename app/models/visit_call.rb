# == Schema Information
#
# Table name: visit_calls
#
#  id         :integer          not null, primary key
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class VisitCall < ActiveRecord::Base
  belongs_to :account
  #Nothing here, but you can extend with status and other fields
end
