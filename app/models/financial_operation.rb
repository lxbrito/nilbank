# == Schema Information
#
# Table name: financial_operations
#
#  id                       :integer          not null, primary key
#  financial_transaction_id :integer
#  account_id               :integer
#  op_type                  :integer          default(0), not null
#  initial_balance          :decimal(18, 2)   default(0.0), not null
#  final_balance            :decimal(18, 2)   default(0.0), not null
#  ammount                  :decimal(18, 2)   default(0.0), not null
#  interest                 :decimal(18, 2)   default(0.0), not null
#  description              :string(255)      default(""), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

class FinancialOperation < ActiveRecord::Base
  belongs_to :financial_transaction
  belongs_to :account

  enum op_type: [:debit, :credit]

  INTEREST_RATE = (0.5)/100
  def concat_operation
    self.account.build_operation(financial_transaction_id, self)
  end

  def transfer_values(parent_op)
    self.initial_balance = parent_op.final_balance
    self.final_balance = parent_op.final_balance
    transfer_interests(parent_op) if account.vip?
    self
  end

  def credit(ammount, description)
    self.credit!
    self.ammount = ammount
    self.final_balance += ammount
    self.description = description
    pay_debts!
    save!
  end

  def debit(ammount, description)
    self.debit!
    self.ammount = ammount
    self.final_balance -= ammount
    self.description = description
    raise InsuficientFundsException unless has_funds?
    save!
  end

  def pay_debts!
    return if interest.zero?
    interest_op = concat_operation
    payment = ammount >= interest ? interest : ammount
    interest_op.interest -= payment
    interest_op.debit(payment, "Pagamento de juros")
  end


  def has_funds?
    account.vip? || self.final_balance >= 0
  end

  private
  def transfer_interests(parent_op)
    self.interest = parent_op.interest
    self.interest += calculate_interests(parent_op) if initial_balance < 0
  end

  def calculate_interests(parent_op)
    elapsed_minutes = (Time.now - (parent_op.financial_transaction&.created_at || Time.now)).truncate.div(60)
    elapsed_minutes * (-initial_balance) * INTEREST_RATE
  end

end
