module Cacheable
  extend ActiveSupport::Concern

  included do
    after_commit :flush_cache
  end

  def flush_cache
    Rails.cache.delete([self.class.name, id])
  end

  # methods defined here are going to extend the class, not the instance of it
  module ClassMethods

    def cached_find(values)
      return nil if values.nil?
      if values.is_a? Array
        return [] if values.empty?
        cached = Rails.cache.fetch_multi(*values, namespace: name, expires_in: 120.minutes) {|value| find(value) }
        cached.values
      else
        Rails.cache.fetch(values, namespace: name, expires_in: 120.minutes) { find(values) }
      end
    end

    def cache_for(name, block, options={})
      ttl = options.fetch(:expires_in, 120.minutes)
      define_method("cached_#{name}") do
        Rails.cache.fetch([self,"#{name}"], expires_in: ttl) do
          block.call(self)
        end
      end

      define_method("flush_#{name}") do
        Rails.cache.delete([self,"#{name}"])
      end

      callback = options.fetch(:flush_at, nil)
      self.send(callback, "flush_#{name}".to_sym) unless callback.nil?
    end

    def ccache_for(name, block, options={})
      ttl = options.fetch(:expires_in, 120.minutes)
      klass = self.to_s
      self.class.instance_eval do
        define_method("cached_#{name}") do
          Rails.cache.fetch([klass,"#{name}"], expires_in: ttl) do
            block.call
          end
        end

        define_method("flush_#{name}") do
          Rails.cache.delete([klass,"#{name}"])
        end
      end

      define_method("flush_#{name}") do
        Rails.cache.delete([self.class.to_s,"#{name}"])
      end

      callback = options.fetch(:flush_at, nil)
      self.send(callback, "flush_#{name}".to_sym) unless callback.nil?
    end

  end

end