# == Schema Information
#
# Table name: accounts
#
#  id                 :integer          not null, primary key
#  name               :string(255)      default(""), not null
#  role               :integer          not null
#  acc_number         :string(255)      default(""), not null
#  encrypted_password :string(255)      default(""), not null
#  sign_in_count      :integer          default(0), not null
#  current_sign_in_at :datetime
#  last_sign_in_at    :datetime
#  current_sign_in_ip :string(255)
#  last_sign_in_ip    :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Account < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable,
  # :recoverable, :rememberable
  has_many :financial_operations
  has_many :stocks
  has_many :visit_calls

  devise :database_authenticatable, :registerable, :trackable, :validatable
  enum role: [:regular, :vip, :manager]

  scope :presentable, -> {where.not(role: roles[:manager])}

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def self.roles_collection
    possible_roles = roles.dup
    possible_roles.delete("manager")
    possible_roles.keys
  end

  def build_operation(t_id, op=last_operation)
    new_operation = financial_operations.build(financial_transaction_id:t_id)
    new_operation.transfer_values(op)
  end

  def last_operation
    financial_operations.last || financial_operations.build
  end

  def transfer_limit
    return 1000.to_d if regular?
    BigDecimal::INFINITY
  end
end
