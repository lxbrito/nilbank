# == Schema Information
#
# Table name: stocks
#
#  id           :integer          not null, primary key
#  account_id   :integer
#  stock_symbol :string(255)      default(""), not null
#  price        :decimal(18, 2)   default(0.0), not null
#  qtd          :integer          default(0), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Stock < ActiveRecord::Base
  belongs_to :account

  def current_price
    @cp ||= Portfolio.cached_price(stock_symbol)
  end

  def current_value
    current_price * qtd
  end
end
