class Transfer
  REGULAR_TRANSFER_FEE = 8.to_d
  VIP_TRANSFER_FEE_RATE = (0.8/100).to_d
  def initialize(origin)
    @origin = origin
  end

  def call(ammount,destination)
    FinancialTransaction.double(@origin, destination) do |op1, op2|
      op1.debit(ammount, "Transferência para conta #{destination.acc_number}")
      op1.concat_operation.debit(transfer_fee(op1), "Taxa de transferência")
      op2.credit(ammount, "Transferência da conta #{@origin.acc_number}")
    end
  end

  private
  def transfer_fee(op1)
    return REGULAR_TRANSFER_FEE if op1.account.regular?
    VIP_TRANSFER_FEE_RATE * op1.ammount
  end
end