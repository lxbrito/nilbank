class Deposit

  def initialize(account)
    @account = account
  end

  def call(ammount)
    FinancialTransaction.single(@account) do |op|
      op.credit(ammount, "Depósito em conta")
    end
  end

end