class Portfolio
  STOCK_SYMBOLS = %w{ PETR4.SA VALE5.SA BVMF3.SA BRML3.SA ALSC3.SA MULT3.SA BBAS3.SA ITSA4.SA }
  YAHOO_URL = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%20in%20(%22STOCKSYMBOL%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="

  def self.cached_price(stock_symbol)
    Rails.cache.fetch(["stock_price", stock_symbol],{expires_in: 1.minute} ) { get_price(stock_symbol)}
  end

  def self.get_price(stock_symbol)
    url = YAHOO_URL.gsub('STOCKSYMBOL', stock_symbol)
    response = RestClient.get(url)
    return if response.net_http_res.code != "200"
    string_price = JSON.parse(response.body)["query"]["results"]["quote"]["LastTradePriceOnly"].to_d
  end


end