class Withdraw
  def initialize(account)
    @account = account
  end

  def call(ammount)
    FinancialTransaction.single(@account) do |op|
      op.debit(ammount, "Saque de conta")
    end
  end
end