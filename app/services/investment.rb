class Investment
  FEE = 1.50.to_d
  COMMISSION_RATE = ((0.5)/100).to_d
  def initialize(account)
    @account = account
  end

  def buy(qtd, stock_symbol)
    FinancialTransaction.single(@account) do |op|
      p "="*120
      p stock_symbol
      stock = Stock.where(account:@account, stock_symbol: stock_symbol).first_or_initialize
      p "="*120
      p stock.stock_symbol
      ammount = qtd * stock.current_price
      stock.qtd += qtd
      stock.price += ammount
      op.debit(ammount, "Compra Ações #{stock.stock_symbol}")
      stock.save!
    end
  end

  def sell(qtd, stock_symbol)
    FinancialTransaction.single(@account) do |op|
      stock = Stock.where(account:@account, stock_symbol: stock_symbol).first_or_initialize
      raise InsuficientStocksException if stock.qtd < qtd
      ammount = qtd * stock.current_price
      individual_price = stock.price/stock.qtd
      stock.qtd -= qtd
      stock.price = (individual_price*stock.qtd).round(2)

      op.credit(ammount, "Venda de ações #{stock.stock_symbol}")

      fee_op = op.concat_operation
      fee_op.debit(FEE, "Taxa sobre venda de ações")

      commision_op = fee_op.concat_operation
      commision_op.debit(selling_commission(ammount), "Comissão sobre venda de ações")
      if stock.qtd.zero?
        stock.destroy!
      else
        stock.save!
      end
    end
  end

  private

  def selling_commission(ammount)
    (ammount * COMMISSION_RATE).round(2) #Ruby 2.4
  end
end