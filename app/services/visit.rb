class Visit
  MANAGER_FEE = 250.to_d
  def initialize(account)
    @account = account
  end

  def call
    FinancialTransaction.single(@account) do |op|
      op.debit(MANAGER_FEE, "Solicitação de visita de gerente")
      VisitCall.create!(account:@account)
    end
  end
end
