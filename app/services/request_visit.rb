class RequestVisit
  VISIT_FEE = 250

  def initialize(account)
    @account = account
  end

  def call
    FinancialTransaction.single(@account) do |op|
      op.debit(VISIT_FEE, "Taxa de visita")
    end
  end
end