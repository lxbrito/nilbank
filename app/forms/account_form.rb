class AccountForm < Reform::Form
  property :name
  property :acc_number
  property :role
  property :password
  property :password_confirmation

  validates :name, presence: true

  validates :acc_number, presence: true, numericality: {greater_than_or_equal_to: 10000}
  validate :acc_number do
    errors.add(:acc_number, "Formato inválido! Número de contas são formados por 5 dígitos.") unless acc_number.match(/\A\d{5}\z/)
  end

  validates :role, inclusion: { in: Account.roles_collection }

  validate :password do
    errors.add(:password, "Não pode ficar em branco.") if !model.persisted? && password.blank?
    errors.add(:password, "Formato inválido! Senhas são formados por 4 dígitos.") unless password.to_s.match(/\A\d{4}?\z/)
  end

  validate :password_confirmation do
    errors.add(:password_confirmation, "Senhas não conferem.") if password && password_confirmation != password
  end
end