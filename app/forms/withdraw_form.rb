class WithdrawForm < Reform::Form
  property :ammount
  validates :ammount, presence: true, numericality: {greater_than_or_equal_to: 0.01}
  validate :ammount do
    errors.add(:ammount, "Não são permitidos valores com mais de 2 casas decimais") unless ammount.match(/\A\d+(?:\.\d{0,2})?\z/)
  end

end