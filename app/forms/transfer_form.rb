class TransferForm < Reform::Form
  property :account
  property :ammount
  property :account_number, virtual:true
  validates :ammount, presence: true, numericality: {greater_than_or_equal_to: 0.01}
  validates :account_number, presence: true, numericality: true

  validate :account_number do
    errors.add(:account_number, "Conta Inválida") if !account_number.match(/\A\d{5}\z/) || destination_account.nil?
    errors.add(:account_number, "Contas origem e destino não podem ser a mesma") if account_number == account.acc_number
  end

  validate :ammount do
    errors.add(:ammount, "Não são permitidos valores com mais de 2 casas decimais") unless ammount.match(/\A\d+(?:\.\d{0,2})?\z/)
    errors.add(:ammount, "Valor acima do seu limite") if ammount.to_d > account.transfer_limit
  end


  def destination_account
    Account.where(acc_number: account_number).first
  end


end