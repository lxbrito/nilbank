class InvestmentForm < Reform::Form
  property :qtd, virtual: true
  property :stock_symbol,virtual: true
  property :ammount
  property :price,virtual: true
  validates :qtd, presence: true, numericality: {greater_than: 0, only_integer:true}

  validates :stock_symbol, inclusion: { in: Portfolio::STOCK_SYMBOLS }
end