class AccountDecorator < Draper::Decorator
  delegate_all

  def lo
    @lo = build_operation(0).decorate
  end

  def formatted_balance
    lo.formatted_balance
  end

  def formatted_visit_calls
    return ['Nenhuma visita solicitada'] if visit_calls.empty?
    visit_calls.map{|vc| "Visita Solicitada em #{vc.created_at}"}
  end

  def formatted_balance_with_interests
    lo.formatted_balance_with_interests
  end

  def formatted_interests
    lo.formatted_interests
  end

  def root_path
    "/#{role}"
  end
end