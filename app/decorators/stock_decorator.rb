class StockDecorator < Draper::Decorator
  delegate_all

  def formatted_price
    format_money(price)
  end

  def formatted_current_price
    format_money(current_price)
  end

  def formatted_current_value
    format_money(current_value)
  end

  def income_percentage
    return "N/A" if price.to_f <= 0
    "#{((current_price*qtd - price)*100 / price).round(2)}%"
  end

  def format_money(value)
    h.number_to_currency(value, unit: "R$", separator: ",", delimiter: ".", negative_format:"R$ (%n)")
  end
end