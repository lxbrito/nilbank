class FinancialOperationDecorator < Draper::Decorator
  delegate_all

  def decorated_account
    account.decorate
  end

  def formatted_balance
    format_money(final_balance)
  end

  def formatted_balance_with_interests
    format_money(final_balance - interest)
  end

  def formatted_interests
    format_money(interest)
  end

  def formatted_ammount
    modifier = credit? ? 1 : -1
    format_money(modifier * ammount)
  end

  def format_money(value)
    h.number_to_currency(value, unit: "R$", separator: ",", delimiter: ".", negative_format:"R$ (%n)")
  end

  def formatted_datetime
    I18n.l financial_transaction.created_at, format: :simple
  end

  def formatted_type
    credit? ? 'Crédito' : 'Débito'
  end

end