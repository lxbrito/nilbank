module ApplicationHelper
  CALLOUT_CLASSES={alert:'alert', notice:'success'}
  def callout_class(key)
    CALLOUT_CLASSES.fetch(key.to_sym, 'primary')
  end
end
