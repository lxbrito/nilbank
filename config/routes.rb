Rails.application.routes.draw do
  devise_for :accounts
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'landing#index'

  namespace :manager do
    resources :accounts
    root 'accounts#index'
  end

  namespace :regular do
    root 'landing#index'
    resources :transfers, only:[:new, :create]
    resources :deposits, only:[:new, :create]
    resources :withdraws, only:[:new, :create]
    resources :statements, only:[:index]
    resources :investments, except:[:delete, :show]
  end

  namespace :vip do
    root 'landing#index'
    resources :transfers, only:[:new, :create]
    resources :deposits, only:[:new, :create]
    resources :withdraws, only:[:new, :create]
    resources :statements, only:[:index]
    resources :investments, except:[:delete, :show]
    resources :visits, only:[:index]
  end
end
