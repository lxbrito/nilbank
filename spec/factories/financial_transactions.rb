# == Schema Information
#
# Table name: financial_transactions
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :financial_transaction do
    account nil
  end
end
