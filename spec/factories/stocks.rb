# == Schema Information
#
# Table name: stocks
#
#  id           :integer          not null, primary key
#  account_id   :integer
#  stock_symbol :string(255)      default(""), not null
#  price        :decimal(18, 2)   default(0.0), not null
#  qtd          :integer          default(0), not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :stock do
    share_code "MyString"
    price "9.99"
    qtd 1
  end
end
