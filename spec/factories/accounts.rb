# == Schema Information
#
# Table name: accounts
#
#  id                 :integer          not null, primary key
#  name               :string(255)      default(""), not null
#  role               :integer          not null
#  acc_number         :string(255)      default(""), not null
#  encrypted_password :string(255)      default(""), not null
#  sign_in_count      :integer          default(0), not null
#  current_sign_in_at :datetime
#  last_sign_in_at    :datetime
#  current_sign_in_ip :string(255)
#  last_sign_in_ip    :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :account do
    
  end
end
