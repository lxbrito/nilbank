# == Schema Information
#
# Table name: financial_operations
#
#  id                       :integer          not null, primary key
#  financial_transaction_id :integer
#  account_id               :integer
#  op_type                  :integer          default(0), not null
#  initial_balance          :decimal(18, 2)   default(0.0), not null
#  final_balance            :decimal(18, 2)   default(0.0), not null
#  ammount                  :decimal(18, 2)   default(0.0), not null
#  interest                 :decimal(18, 2)   default(0.0), not null
#  description              :string(255)      default(""), not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#

require 'rails_helper'

RSpec.describe FinancialOperation, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
