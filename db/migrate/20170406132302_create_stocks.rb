class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks do |t|
      t.references :account, index: true, foreign_key: true
      t.string :stock_symbol, null: false, default:""
      t.decimal :price, null: false, default:0, precision: 18, scale: 2
      t.integer :qtd, null: false, default:0

      t.timestamps null: false
    end
  end
end
