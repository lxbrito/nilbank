class CreateFinancialTransactions < ActiveRecord::Migration
  def change
    create_table :financial_transactions do |t|

      t.timestamps null: false
    end
  end
end
