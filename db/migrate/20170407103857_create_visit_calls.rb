class CreateVisitCalls < ActiveRecord::Migration
  def change
    create_table :visit_calls do |t|
      t.references :account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
