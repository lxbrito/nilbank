class CreateFinancialOperations < ActiveRecord::Migration
  def change
    create_table :financial_operations do |t|
      t.references :financial_transaction, index: true, foreign_key: true
      t.references :account, index: true, foreign_key: true
      t.integer :op_type, null: false, default:0
      t.decimal :initial_balance, null: false, default:0, precision: 18, scale: 2
      t.decimal :final_balance, null: false, default:0, precision: 18, scale: 2
      t.decimal :ammount, null: false, default:0, precision: 18, scale: 2
      t.decimal :interest, null: false, default:0, precision: 18, scale: 2
      t.string :description, null: false, default:""

      t.timestamps null: false
    end
  end
end
