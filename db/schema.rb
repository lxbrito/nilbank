# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170407103857) do

  create_table "accounts", force: :cascade do |t|
    t.string   "name",               limit: 255, default: "", null: false
    t.integer  "role",               limit: 4,                null: false
    t.string   "acc_number",         limit: 255, default: "", null: false
    t.string   "encrypted_password", limit: 255, default: "", null: false
    t.integer  "sign_in_count",      limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip", limit: 255
    t.string   "last_sign_in_ip",    limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "accounts", ["acc_number"], name: "index_accounts_on_acc_number", unique: true, using: :btree

  create_table "financial_operations", force: :cascade do |t|
    t.integer  "financial_transaction_id", limit: 4
    t.integer  "account_id",               limit: 4
    t.integer  "op_type",                  limit: 4,                            default: 0,   null: false
    t.decimal  "initial_balance",                      precision: 18, scale: 2, default: 0.0, null: false
    t.decimal  "final_balance",                        precision: 18, scale: 2, default: 0.0, null: false
    t.decimal  "ammount",                              precision: 18, scale: 2, default: 0.0, null: false
    t.decimal  "interest",                             precision: 18, scale: 2, default: 0.0, null: false
    t.string   "description",              limit: 255,                          default: "",  null: false
    t.datetime "created_at",                                                                  null: false
    t.datetime "updated_at",                                                                  null: false
  end

  add_index "financial_operations", ["account_id"], name: "index_financial_operations_on_account_id", using: :btree
  add_index "financial_operations", ["financial_transaction_id"], name: "index_financial_operations_on_financial_transaction_id", using: :btree

  create_table "financial_transactions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stocks", force: :cascade do |t|
    t.integer  "account_id",   limit: 4
    t.string   "stock_symbol", limit: 255,                          default: "",  null: false
    t.decimal  "price",                    precision: 18, scale: 2, default: 0.0, null: false
    t.integer  "qtd",          limit: 4,                            default: 0,   null: false
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
  end

  add_index "stocks", ["account_id"], name: "index_stocks_on_account_id", using: :btree

  create_table "visit_calls", force: :cascade do |t|
    t.integer  "account_id", limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "visit_calls", ["account_id"], name: "index_visit_calls_on_account_id", using: :btree

  add_foreign_key "financial_operations", "accounts"
  add_foreign_key "financial_operations", "financial_transactions"
  add_foreign_key "stocks", "accounts"
  add_foreign_key "visit_calls", "accounts"
end
